'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-minify-css');
var prefix = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifyHTML = require('gulp-minify-html');

gulp.task('scss', function () {
  gulp.src('./scss/**/*.scss')
    .pipe(sass({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(prefix("last 1 version", "> 1%", "ie 8", "ie 7"))
    .pipe(gulp.dest('./css'))
    .pipe(minifyCSS({
      processImport: false
    }))
    .pipe(gulp.dest('./css-min'));
});

gulp.task('scripts', function() {
  gulp.src(['./js/stomp.js', './js/libs.js', './js/widget.js'])
    .pipe(concat('widget.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./js-min'))
});

gulp.task('templates', function() {
  gulp.src('./views/**/*.html')
    .pipe(minifyHTML())
    .pipe(gulp.dest('./views-min'));
});

// Rerun the task when a file changes
gulp.task('scss:watch', function () {  gulp.watch('./scss/**/*.scss', ['scss']);  });
gulp.task('scripts:watch', function () {  gulp.watch('./js/**/*.js', ['scripts']);  });
gulp.task('templates:watch', function () {  gulp.watch('./views/**/*.html', ['templates']);  });

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['scss:watch', 'scripts:watch', 'templates:watch']);
