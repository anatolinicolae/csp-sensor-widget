// Widget boorkmarklet
// Smart bench
javascript: void((function() {
  var e = document.getElementById("meteo"), s = document.createElement("script");

  e.innerHTML='<div class="csp-sensor-widget" data-sensor="quadrante.b3fe0f7e-5cc6-45e3-8b3f-3fa2cb41537c" data-stream="Meteogiusiana" data-dataset="361"></div>';
  e.style.width="200px";

  s.setAttribute("src", "http://s4.squared.cf/widget_production/js-min/widget.js");
  document.head.appendChild(s);
})());

// Traffic flow
javascript: void((function() {
  var e = document.getElementById("meteo"), s = document.createElement("script");

  e.innerHTML='<div class="csp-sensor-widget" data-sensor="sandbox.2f9454e7-4234-40dd-cce1-1d8cab61a4e5" data-stream="TrFl" data-dataset="359"></div>';
  e.style.width="200px";

  s.setAttribute("src", "http://s4.squared.cf/widget_production/js-min/widget.js");
  document.head.appendChild(s);
})());

// Index
javascript: void((function() {
  var e = document.getElementById("meteo"), s = document.createElement("script");

  e.innerHTML='<div class="csp-sensor-widget" data-sensor="quadrante.b3fe0f7e-5cc6-45e3-8b3f-3fa2cb41537c" data-type="index"></div>';
  e.style.width="200px";

  s.setAttribute("src", "http://s4.squared.cf/widget_production/js-min/widget.js");
  document.head.appendChild(s);
})());

// Smart bench iframe
javascript: void((function() {
  var e = document.getElementById('meteo'), s = document.createElement('iframe');

  s.setAttribute('width', '200');
  s.setAttribute('height', '415');
  s.setAttribute('frameborder', '0');
  s.setAttribute('src', 'http://s4.squared.cf/widget_iframe/widget.php?sensor=quadrante.b3fe0f7e-5cc6-45e3-8b3f-3fa2cb41537c&stream=Meteogiusiana&dataset=361');
  
  e.innerHTML = '';
  e.appendChild(s);
})());

// Traffic flow iframe
javascript: void((function() {
  var e = document.getElementById('meteo'), s = document.createElement('iframe');

  s.setAttribute('width', '200');
  s.setAttribute('height', '250');
  s.setAttribute('frameborder', '0');
  s.setAttribute('src', 'http://s4.squared.cf/widget_iframe/widget.php?sensor=sandbox.2f9454e7-4234-40dd-cce1-1d8cab61a4e5&stream=TrFl&dataset=359');
  
  e.innerHTML = '';
  e.appendChild(s);
})());

// Index iframe
javascript: void((function() {
  var e = document.getElementById('meteo'), s = document.createElement('iframe');

  s.setAttribute('width', '200');
  s.setAttribute('height', '295');
  s.setAttribute('frameborder', '0');
  s.setAttribute('src', 'http://s4.squared.cf/widget_iframe/widget.php?sensor=quadrante.b3fe0f7e-5cc6-45e3-8b3f-3fa2cb41537c&type=index');

  e.innerHTML = '';
  e.appendChild(s);
})());