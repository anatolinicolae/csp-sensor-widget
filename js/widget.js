/*! Sensor Widget v0.1.21 | (c) 2015 Anatoli Nicolae */
/*! (c) 2015 CSP Innovazione nelle ICT | www.csp.it */

var Sensor = function() {
  // Assets URL
  this.ASSETS = 'http://localhost/widget_production/';

  // WebSocket endpoint
  this.WSS = 'ws://stream.smartdatanet.it/ws/';
  // OData endpoint
  this.OData = 'http://api.smartdatanet.it/api/';
  // Forecast endpoint
  this.forecast = this.ASSETS + 'data-static/forecast.php';

  // STOMP client instance
  this.client = null;
  // WebSocket connection status (0 not connected, 1 loading, 2 connected)
  this.connected = 0;
  // Templates container - Load once, use anytime.
  this.templates = {};
  // Start processing
  this.start();
};

Sensor.prototype = {
  // Get current instance of the client
  get_client: function() {
    return this.client;
  },

  // Set a new instance of the client
  set_client: function(client) {
    this.client = client;
  },

  // Init method
  start: function() {
    var that = this;

    // Load CSS to head
    var r = document.createElement('link');
    r.setAttribute('rel', 'stylesheet');
    r.setAttribute('type', 'text/css');
    r.setAttribute('href', this.ASSETS + 'css-min/widget.css');
    document.head.appendChild(r);

    // Connect to the socket
    this.connect(function() {
      console.log('Connected to WS');
    });

    // Select our widgets
    var widgets = document.getElementsByClassName('csp-sensor-widget');
    for (i = 0; i < widgets.length; i++) {
      // Select single widget
      var widget = widgets[i];

      // Don't load again if already rendered
      if (!widget.hasAttribute('data-render-status') &&
          !(widget.getAttribute('data-render-status') == 'loading') ||
           (widget.getAttribute('data-render-status') == 'done')
         ) {

        // Extract widget params
        var options = {
          sensor: widget.getAttribute('data-sensor'),
          stream: widget.getAttribute('data-stream'),
          dataset: widget.getAttribute('data-dataset')
        };

        // Start loading
        widget.innerHTML = 'Loading...';

        // Add render status attribute to prevent API other requests
        widget.setAttribute('data-render-status', 'loading');

        // If it's a sensor stream
        if (widget.hasAttribute('data-stream')) {
          // Load latest data
          this.odata_load(widget, options);

          // Subsribe to the stream
          this.subscribe(widget, options);
        } else {
          // Load static forecast
          this.load_forecast(widget, options);
        }
      }
    }
  },

  // Connect to WebSocket
  connect: function(callback) {
    var that = this;

    // If not connected yet
    if (this.get_client() == null || this.connected < 1) {
      // Change status to loading
      this.connected = 1;

      // Create a new socket connection
      this.set_client(Stomp.client(this.WSS));

      // Disable debug log
      this.get_client().debug = false;

      // Log in as user
      this.get_client().connect('guest', 'Aekieh6F', function() {
        // Change status to connected
        that.connected = 2;
        // Call the function
        callback();
      }, function(frame) {
        // Log the error
        console.debug('Error.');
      });
    } else {
      // Call the function
      callback();
    }
  },

  // Subscribe to a new feed
  subscribe: function(widget, options) {
    var that = this;

    // Wait to establish connection
    this.connection_wait(function() {
      // Build endpoint URL
      feed = '/topic/output.' + options.sensor + '_' + options.stream;

      // Subscribe
      that.get_client().subscribe(feed,
        function(message) {
          // Execute on socket message
          that.socket_message(widget, message);
        }
      );
    });
  },

  // Wait for the connection being established
  connection_wait: function(callback) {
    // Store current instance for callback
    var that = this;

    // Check if connected every 100ms, then callback
    var check = setInterval(function() {
      // If not connected
      if (that.connected < 2) {
        // Still waiting
        console.debug('Waiting...');
      } else {
        // Clear loopy block
        clearInterval(check);
        // Call the function
        callback();
      }
    }, 100);
  },

  // On socket message
  socket_message: function(widget, message) {
    // Get sensor data and parse message
    var sensor = widget.getAttribute('data-sensor'),
        obj = JSON.parse(message.body);

    // If traffic flow sensor
    if (sensor == 'sandbox.2f9454e7-4234-40dd-cce1-1d8cab61a4e5') {
      // Get car number and update elements
      var car_number = widget.querySelector('.sensor__data .icon__car'),
          updated_at = widget.querySelector('.sensor__lastupdate');

      // Change car number to sum of the two flows
      // of the camera and add unit element
      car_number.innerHTML = parseInt(obj.values[0].components.flusso1) +
                             parseInt(obj.values[0].components.flusso2) +
                             '<span class="icon__unit"> /min</span>';

      // Change updated time
      updated_at.innerHTML = 'Updated at: ' +
                             this.makeDate(Date.parse(obj.values[0].time));
    }

    // If it's a bench sensor
    if (sensor == 'quadrante.b3fe0f7e-5cc6-45e3-8b3f-3fa2cb41537c') {
      // Get data display elements
      var temp = widget.querySelector('.sensor__data .icon__temperature'),
          humidity = widget.querySelector('.sensor__data .icon__humidity'),
          pressure = widget.querySelector('.sensor__data .icon__pressure'),
          people = widget.querySelector('.sensor__data .icon__people'),
          updated_at = widget.querySelector('.sensor__lastupdate');

      // Change data
      temp.innerHTML = parseFloat(obj.values[0].temperatura).toFixed(1) +
                       '<span class="icon__unit">°C</span>';
      humidity.innerHTML = parseFloat(obj.values[0].umidita).toFixed(1) +
                         '<span class="icon__unit">%</span>';
      pressure.innerHTML = parseInt(obj.values[0].pressione) +
                         '<span class="icon__unit">hPa</span>';
      people.innerHTML = (parseInt(obj.values[0].passaggi) || 'null') +
                         '<span class="icon__unit">/h</span>';

      // Change updated time
      updated_at.innerHTML = 'Updated at: ' +
                             this.makeDate(Date.parse(obj.values[0].time));
    }
  },

  // Load OData
  odata_load: function(widget, options) {
    var that = this,
        // Build endpoint URL
        endpoint = this.OData + 'ds_';
        endpoint += options.stream.capitalize().substring(0, 12);
        endpoint += '_' + options.dataset + '/Measures';

    // GET latest data
    ajax.get(
      endpoint,
      // Query
      {
        '$format': 'json',
        '$top': 1,
        '$orderby': 'time desc'
      },
      // On response
      function(responseText) {
        // Parse response
        var response = JSON.parse(responseText);
        // and process it
        that.process_odata(widget, options, response);
      },
      // Async? OFC!
      true
    );
  },

  // Process OData
  process_odata: function(widget, options, response) {
    var that = this,
        // Select first result
        response = response.d.results[0];

    // Strip time
    var time = response.time.replace('/Date(', '');
    time = parseInt(time.replace(')/', ''), 10);

    // Clean response
    delete response.__metadata;
    delete response.time;
    delete response.internalId;
    delete response.datasetVersion;

    // Select template
    var template = 'template-';

    // If it's a traffic flow sensor
    if (options.sensor == 'sandbox.2f9454e7-4234-40dd-cce1-1d8cab61a4e5') {
      template += 'cars';
    }

    // If it's a bench sensor
    if (options.sensor == 'quadrante.b3fe0f7e-5cc6-45e3-8b3f-3fa2cb41537c') {
      template += 'bench';
    }

    // If template not loaded and not loading
    if (!(template in this.templates) &&
          this.templates[template] != 'loading') {
      // Change status to loading
      this.templates[template] = 'loading';
      // GET the template
      ajax.get(this.ASSETS + 'views-min/' + template + '.html', {},
        // On response
        function(responseText) {
          // Load template into templates var
          that.templates[template] = responseText;
          // Process template and display the widget
          that.display_widget(widget, template, response, time);
        },
      // Async? OFC!
      true);
    } else {
      // Process template and display the widget
      this.display_widget(widget, template, response, time);
    }
  },

  // Process forecast data
  process_forecast: function(widget, options, response) {
    var that = this,
        // Select first set of values
        response = response.values[0],
        // Parse time
        time = Date.parse(response.time);

    // Select template
    var template = 'template-meter';

    // If template not loaded and not loading
    if (!(template in this.templates) &&
          this.templates[template] != 'loading') {
      // Change status to loading
      this.templates[template] = 'loading';
      // GET the template
      ajax.get(this.ASSETS + 'views-min/' + template + '.html', {},
        // On response
        function(responseText) {
          // Load template into templates var
          that.templates[template] = responseText;
          // Process template and display the widget
          that.display_widget(widget, template, response, time);
        },
      // Async? OFC!
      true);
    } else {
      // Process template and display the widget
      this.display_widget(widget, template, response, time);
    }
  },

  // Load forecast data
  load_forecast: function(widget, options) {
    var that = this;
    // GET forecast
    ajax.get(this.forecast, {},
      // On response
      function(responseText) {
        // Parse response
        var response = JSON.parse(responseText);
        // and process it
        that.process_forecast(widget, options, response);
      },
    // Async? OFC!
    true);
  },

  // Display the widgets
  display_widget: function(widget, template, oDataResult, time) {
    var that = this, name, location, options = {};

    // Check if teplate loaded
    var loadedTemplate = setInterval(function() {
      // If template loaded
      if (that.templates[template] != 'loading') {
        // Clear loopy block
        clearInterval(loadedTemplate);

        // If template is traffic flow one
        if (template == 'template-cars') {
          // Set template data
          name = 'Montalto Dora';
          location = 'Montalto Dora TO';
          options = {
            name: name,
            location: location,
            car_number: parseFloat(oDataResult['flusso1']) +
                        parseFloat(oDataResult['flusso2']),
            lat: 45.448829,
            lng: 7.887703,
            zoom: 14,
            updated_at: that.makeDate(time)
          };
        }

        // If template is bench one
        if (template == 'template-bench') {
          // Set template data
          name = 'Giusiana';
          location = 'Lungo Dora, Corso Re Umberto, 10015, Ivrea';
          options = {
            name: name,
            location: location,
            temperature: parseFloat(oDataResult.temperatura).toFixed(1),
            humidity: parseFloat(oDataResult.umidita).toFixed(1),
            pressure: parseInt(oDataResult.pressione),
            people: parseInt(oDataResult.passaggi) || 'null',
            lat: 45.465588,
            lng: 7.879913,
            zoom: 15,
            updated_at: that.makeDate(time)
          };
        }

        // If template is air quality meter
        if (template == 'template-meter') {
          // Set template data
          name = 'Qualità aria';
          location = 'Montalto Dora TO';
          options = {
            name: name,
            location: location,
            forecast: oDataResult.components.forecast,
            lat: 45.465588,
            lng: 7.879913,
            zoom: 15,
            updated_at: that.makeDate(time)
          };
        }

        // Store a temporary widget copy
        var temp = widget;
        // Remove contents
        temp.innerHTML = '';
        // Remove dynamically added attributes
        temp.removeAttribute('data-render-status');

        // Set embed text
        options['embed'] = temp.outerHTML + '<script src="' +
                           that.ASSETS + 'js-min/widget.js"></script>';

        // Load template with options into the engine
        output = TemplateEngine(that.templates[template], options);

        // Replace contents with template parse by the engine
        widget.innerHTML = output;

        // Attach click action to info icon
        widget.querySelector('.project__info').onclick = function() {
          // Hide home section
          widget.querySelector('.widget__home').style.display = 'none';
          // Show info section
          widget.querySelector('.widget__info').style.display = 'block';
        };

        // Attach click action to embed icon
        widget.querySelector('.project__embed').onclick = function() {
          // Hide home section
          widget.querySelector('.widget__home').style.display = 'none';
          // Show embed section
          widget.querySelector('.widget__embed').style.display = 'block';
        };

        // Select all location elements
        var elements = widget.querySelectorAll('.sensor__location');
        Array.prototype.forEach.call(elements, function(el, i) {
          // Attach for each one click action
          el.onclick = function() {
            // Hide other sections
            widget.querySelector('.widget__info').style.display = 'none';
            widget.querySelector('.widget__embed').style.display = 'none';
            widget.querySelector('.widget__home').style.display = 'none';
            // Display map section
            widget.querySelector('.widget__map').style.display = 'block';
          };
        });

        // Select all close elements
        var elements = widget.querySelectorAll('.sensor__closeview');
        Array.prototype.forEach.call(elements, function(el, i) {
          // Attach for each one click action
          el.onclick = function() {
            // Hide other sections
            widget.querySelector('.widget__info').style.display = 'none';
            widget.querySelector('.widget__embed').style.display = 'none';
            widget.querySelector('.widget__map').style.display = 'none';
            // Display home section
            widget.querySelector('.widget__home').style.display = 'block';
          };
        });

        // Get location element height
        var h = widget.querySelector('.sensor__location').offsetHeight;
        // If element height > 30px and widget height > 300px
        if (h > 30 && widget.offsetHeight > 300) {
          // Calculate new height of the widget
          h = widget.offsetHeight + (h - 28) + 'px';
          // Set new height to widget
          widget.style.height = h;
          Array.prototype.forEach.call(widget.querySelectorAll('section'),
            function(el, i) {
              // and to widget's sections
              el.style.maxHeight = h;
            }
          );
        }

        // If template is air quality meter
        if (template == 'template-meter') {
          // Set old and new ranges
          var OldMin = 0,
              OldMax = 100,
              NewMin = 1,
              NewMax = 2,
              val = '#55aa00';
          // Set canvas dimensions
          this.centerX = 150;
          this.centerY = 150;
          // Get gauge canvas context
          // Select gauge ccanvas element
          this.c = widget.querySelector('.meter__gauge');
          // Get canvas context
          this.ctx = c.getContext('2d');

          // Switch colors by range
          if (oDataResult.components.forecast < 35) {
            val = '#00ff00';
          } else if (oDataResult.components.forecast < 50) {
            val = '#ffff00';
          } else if (oDataResult.components.forecast < 100) {
            val = '#ff0000';
          } else if (oDataResult.components.forecast < 500) {
            val = '#aa00ff';
          }

          // Draw full range arc
          that.drawArc(this, 'rgba(255, 255, 255, 0.15)', 1, 2);
          // Draw current value arc
          that.drawArc(this, val, 1, that.r2r(OldMin, OldMax,
                                              NewMin, NewMax,
                                              oDataResult.components.forecast));
          // Select gauge text element
          var el = widget.querySelector('.meter__value');
          // Change text with current value
          el.innerHTML = oDataResult.components.forecast +
                         '<span>µg/m&sup3;</span>';
        }

        // Add render status attribute
        widget.setAttribute('data-render-status', 'done');

      }
    }, 10);
  },

  // Add zero to a < 10 number
  addZero: function(i) {
    if (i < 10) {
      i = '0' + i;
    }
    return i;
  },

  // Make normalized date from timestamp
  makeDate: function(date) {
    // Get new date
    date = new Date(date);
    // Make output
    date = this.addZero(date.getHours()) + ':' +
           this.addZero(date.getMinutes()) + ' ' +
           this.addZero(date.getDate()) + '/' +
           this.addZero(date.getMonth() + 1);
    // Return it
    return date;
  },

  // Switch from range to range
  r2r: function(OldMin, OldMax, NewMin, NewMax, OldValue) {
    // stackoverflow.com/a/929107
    return (
      ((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)
      ) + NewMin;
  },

  // Draw an arc
  drawArc: function(that, color, start, end) {
    // Get context
    ctx = that.ctx;
    // Set color
    ctx.strokeStyle = color;
    // Set line width
    ctx.lineWidth = 6;
    // Prepare for drawing
    ctx.beginPath();
    // Make an arc
    ctx.arc(
        that.centerX,
        that.centerY,
        that.c.width / 2 - 6,
        Math.PI * start,
        Math.PI * end, false
    );
    // Apply stroke to path
    ctx.stroke();
  }
};

(function() {
  // Create a new istance of the widget
  if (!window.SensorWidget) {
    window.SensorWidget = new Sensor();
  }
})();
